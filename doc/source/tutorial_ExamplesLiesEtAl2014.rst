Example from SSA paper
======================


This part of the documentation contains an example for the SSA algorithm presented in [LiesEtAl2014]_.
The code of the example is also located in the Examples directory.

Simple SSA example
------------------
This example shows how to apply slow subspace analysis (SSA) on image patches, including the preprocessing steps of projecting out the DC component and apply whitening on the k-1 dimensional remaining subspace.
*Remark*: due to licencing reasons, we cannot provide the data used in the example. See the data sampling or loading examples on how to obtain data.

from ``Examples/SSA_example.py``:

.. literalinclude:: ../../Examples/SSA_example.py

.. [LiesEtAl2014] Joern-Philipp Lies, Ralf M. Haefner, and 
   Matthias Bethge, *Slowness and sparseness have diverging effects on complex cell coding*, PLoS Computational Biology, 10(3), 2014

