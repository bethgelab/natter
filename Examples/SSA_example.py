# coding: utf-8
from __future__ import division
import numpy as np

import natter
from natter.DataModule import Data, DataSampler, DataLoader
from natter.Distributions import Uniform
from natter.Transforms import LinearTransformFactory as LTF, LinearTransform

#Loading 11x11 patches. Odd patch sizes work best, as we project out the 
#DC component and thus remain with an even number of dimensions.
dat = DataLoader.load('got100k_11x11.pydat')
print(dat)

#Creating the DFT-Basis where the vectors are grouped in quadrature pairs
#+ DC component as first basis vector
Ufft = LTF.DCACQuadraturePairs2D(int(np.sqrt(dat.X.shape[0])))

#Reduction matrix which projects out the DC component
R = Ufft[1:,:]

#Symmetric whitening on the reduced space
W = LTF.SYM(R*dat)

#Projecting out the DC component and applying the symmetric whitening to create the training data
dat_white = W*R*dat
print(dat_white)

#Calculating the slow subspace analysis
U = LTF.SSA(dat_white, verbose=1)

#Transforming the SSA basis back to pixel space
F = (R.T()*W*U).T()

#Plotting the learned filters
F.plotFilters()



